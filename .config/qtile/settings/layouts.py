# Antonio Sarosi
# https://youtube.com/c/antoniosarosi
# https://github.com/antoniosarosi/dotfiles

from libqtile import layout
from libqtile.config import Match
from .widgets import colors

# Layouts and layout rules


layout_conf = {
    'border_focus': colors['focus'],
    'border_width': 3,
    'border_normal': colors['inactive'],
    'margin': 9
}

layouts = [
    layout.MonadTall(**layout_conf),
    layout.Max(),
    layout.MonadWide(**layout_conf),
    layout.Bsp(**layout_conf),
    layout.Matrix(columns=2, **layout_conf),
    layout.RatioTile(**layout_conf),
    # layout.Columns(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

floating_layout = layout.Floating(
    float_rules=[
        *layout.Floating.default_float_rules,
        Match(wm_class='confirmreset'),
        Match(wm_class='lxappearance'),
        Match(wm_class='nitrogen'),
        Match(wm_class='makebranch'),
        Match(wm_class='maketag'),
        Match(wm_class='ssh-askpass'),
        Match(wm_class='qalculate-gtk'),
        Match(wm_class='Unity'),
        Match(wm_class='mpv'),
        Match(title='branchdialog'),
        Match(title='pinentry'),
    ],
    **layout_conf
)
