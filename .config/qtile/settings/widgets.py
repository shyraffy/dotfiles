from libqtile import widget, qtile
import os

# Get the icons at https://www.nerdfonts.com/cheat-sheet (you need a Nerd Font)


colors = {
    "grey": "#353c4a",
    "dark": '#282c34',
    "highlight": ["#282c34", "#282c34"],
    # "dark": "#2D2A2E",
    "light": "#f1ffff",
    "text": "#0f101a",
    # "focus": "#a151d3",
    "focus": "#96bfe4",
    "active": "#ffffff",
    "inactive": "#4c566a",
    "urgent": "#F07178",
    "color1": "#a151d3",
    "color2": "#F07178",
    "color3": "#fb9f7f",
    "color4": "#ffd47e",
}

def base(fg='text', bg='dark'): 
    return {
        'foreground': colors[fg],
        'background': colors[bg]
    }


def separator():
    return widget.Sep(**base(), linewidth=0, padding=5)


def icon(fg='text', bg='dark', fontsize=16, text="?"):
    return widget.TextBox(
        **base(fg, bg),
        fontsize=fontsize,
        text=text,
        padding=3
    )

def divider(div='    ', size='22'):
    return widget.TextBox(
        background=colors['dark'],
        foreground=colors['inactive'],
        text=div,
        font="ShureTechMono Nerd Font",
        fontsize=size
    )

def workspaces(): 
    return [
        widget.GroupBox(
            **base(fg='light'),
            font='UbuntuMono Nerd Font',
            fontsize=19,
            margin_y=3,
            margin_x=0,
            padding_y=8,
            padding_x=5,
            borderwidth=5,
            active=colors['active'],
            inactive=colors['inactive'],
            rounded=False,
            highlight_method='line',
            highlight_color = colors['highlight'],
            urgent_alert_method='block',
            urgent_border=colors['urgent'],
            this_current_screen_border=colors['focus'],
            this_screen_border=colors['grey'],
            other_current_screen_border=colors['dark'],
            other_screen_border=colors['dark'],
            disable_drag=True
        ),
        divider(div='|'),

        widget.CurrentLayoutIcon(
            **base(fg='focus'), 
            scale=0.65,
            custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")]
        ),
        widget.CurrentLayout(**base(fg='light'), padding=5),

        divider(div='|'),

        widget.WindowName(**base(fg='light'), format='{name}'),

        separator(),
    ]


primary_widgets = [

    # widget.TextBox(
    #     text="  ",
    #     fontsize=24,
    #     font="UbuntuMono Nerd Font Bold",
    #     background=colors['dark'],
    #     foreground=colors['light'],
    #     mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('alacritty')}
    # ),
    #
    # divider(div=' |'),
    #
    # widget.TextBox(
    #     **base('active', 'dark'),
    #     text="",
    #     fontsize=55,
    #     padding=-4,
    #     font="UbuntuMono Nerd Font Bold"
    # ),
    #
    # separator(),

    *workspaces(),

    separator(),

    widget.Systray(background=colors['dark'], padding=5, icon_size=24),

    divider(size='10'),

    icon(fg="focus", text=' '), 

    widget.CPU(
        **base(fg='light'), 
        format='{load_percent}%',
        update_interval=3.0,
        padding=5
    ),

    divider(size='10'),

    # icon(fg="focus", text='  '), 
    #
    # widget.Memory(
    #     **base(fg='light'),
    #     format='{MemUsed: .0f}{mm} /{MemTotal: .0f}{mm} ',
    #     update_interval=3.0,
    # ),
    #
    #
    # divider(size='10'),

    icon(fg='focus', text=' '), # Icon: nf-fa-download
    
    widget.CheckUpdates(
        background=colors['dark'],
        colour_have_updates=colors['light'],
        colour_no_updates=colors['light'],
        no_update_string='0',
        display_format=' {updates}',
        update_interval=1800,
        custom_command='checkupdates',
		mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('alacritty -e sudo pacman -Syyu')},
    ),

    divider(size='10'),

    # icon(bg="color3", text=' '),  # Icon: nf-fa-feed
    icon(fg="focus", text=' '),  # Icon: nf-fa-wifi
    
    widget.Net(
        **base(fg='light'), 
        interface='enp0s31f6', 
        format=' {down} ↓ {up} ↑ ',
    ),

    divider(size='10'),

    icon(fg="focus", fontsize=17, text=' '), # Icon: nf-mdi-calendar_clock

    widget.Clock(
        **base(fg='light'), 
        format='%d/%m/%Y - %H:%M',
    ),

    divider(size='10'),

    widget.TextBox(
        text="襤 ",
        fontsize=25,
		background=colors['dark'],
		mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('powermenu')}
    ),
]

secondary_widgets = [
    *workspaces(),

    separator(),

    icon(fg="focus", fontsize=17, text=' '), # Icon: nf-mdi-calendar_clock

    widget.Clock(**base(fg='light'), format='%d/%m/%Y - %H:%M '),

]

widget_defaults = {
    # 'font': 'ShureTechMono Nerd Font Bold',
    'font' : 'UbuntuMono Nerd Font Bold',
    'fontsize': 18,
    'padding': 1,
}
extension_defaults = widget_defaults.copy()
