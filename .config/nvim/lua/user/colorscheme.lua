local colorscheme = "onedark"

local status_ok, config = pcall(require, "user.colorschemes." .. colorscheme)
if not status_ok then
  vim.notify("colorscheme config for " .. colorscheme .. " not found!")
else
    local theme = require(colorscheme)
    theme.setup(config)
end

local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not status_ok then
  vim.notify("colorscheme " .. colorscheme .. " not found!")
  return
end

