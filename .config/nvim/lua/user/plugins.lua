local fn = vim.fn

-- Automatically install packer
-- ~/.local/share/nvim
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  }
  print "Installing packer close and reopen Neovim..."
  vim.cmd [[packadd packer.nvim]]
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
-- Everytime a BufWritePost happens with plugins.lua (saving the file),
-- this sources the file and pipe it to PackerSync
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

-- Use a protected call so we don't error out on first use
-- We would normally do local packer = requiere("packer")
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end

-- Have packer use a popup window
packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float { border = "rounded" }
    end,
  },
}

-- Install your plugins here
return packer.startup(function(use)
  --
  -- My plugins here
  use "wbthomason/packer.nvim" -- Have packer manage itself
  use "nvim-lua/popup.nvim" -- An implementation of the Popup API from vim in Neovim
  use "nvim-lua/plenary.nvim" -- Useful lua functions used ny lots of plugins
  use "windwp/nvim-autopairs" -- Automatically close parenthesis
  use "goolord/alpha-nvim" -- Greeter for nvim
  use "akinsho/bufferline.nvim" -- Display buffer windows at the top
  use "moll/vim-bbye" -- close buffers with :Bdelete
  use 'norcalli/nvim-colorizer.lua' -- Colorize hex colors

  use {
      'numToStr/Comment.nvim',
      config = function()
          require('Comment').setup()
      end
  } -- Comment and uncomment with gcc

 -- cmp plugins
  use "hrsh7th/nvim-cmp" -- The completion plugin
  use "hrsh7th/cmp-buffer" -- buffer completions
  use "hrsh7th/cmp-path" -- path completions
  use "hrsh7th/cmp-cmdline" -- cmdline completions
  use "saadparwaiz1/cmp_luasnip" -- snippet completions for lua
  use "hrsh7th/cmp-nvim-lsp"
  use "hrsh7th/cmp-nvim-lua"

  -- Nvim tree
  use "kyazdani42/nvim-web-devicons"
  use {
    "kyazdani42/nvim-tree.lua",
    commit = "0f0f858348aacc94f98ba32880760c5a5440b825"
  }

  -- snippets
  use "L3MON4D3/LuaSnip" --snippet engine
  use "rafamadriz/friendly-snippets" -- a bunch of snippets to use

  -- LSP
  use "neovim/nvim-lspconfig"
  use "williamboman/mason.nvim"
  use "williamboman/mason-lspconfig.nvim"
  use 'jose-elias-alvarez/null-ls.nvim' -- LSP diagnostics and code actions

  -- Treesitter
  use {
   "nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate",
  }
  use "p00f/nvim-ts-rainbow"

  -- Guides for indentation
  use "lukas-reineke/indent-blankline.nvim"
  require('indent_blankline').setup {
    char = '┊',
    show_trailing_blankline_indent = false,
  }

  -- Telescope
  use({
      "nvim-telescope/telescope.nvim",
      requires = { { "kdheepak/lazygit.nvim" } }
  })

  -- git
  use "lewis6991/gitsigns.nvim"

  -- Colorschemes
  use 'navarasu/onedark.nvim'
  use 'tanvirtin/monokai.nvim'
  use 'folke/tokyonight.nvim'
  use 'ellisonleao/gruvbox.nvim'
  use "rebelot/kanagawa.nvim"

  -- Bar
  use "nvim-lualine/lualine.nvim"

  -- Terminal
  use "akinsho/toggleterm.nvim"

  -- Latex preview
  use 'lervag/vimtex'

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if PACKER_BOOTSTRAP then
    require("packer").sync() end
end)
