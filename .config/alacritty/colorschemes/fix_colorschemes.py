
import re

def parse_color_schemes(content):
    schemes = {}
    current_scheme = None
    current_section = None

    for line in content.splitlines():
        scheme_match = re.match(r'\[schemes\.(\w+)\.(\w+)\]', line)
        if scheme_match:
            current_scheme, current_section = scheme_match.groups()
            if current_scheme not in schemes:
                schemes[current_scheme] = {}
            schemes[current_scheme][current_section] = []
        elif line.strip() and current_scheme and current_section:
            schemes[current_scheme][current_section].append(line.strip())
    
    return schemes

def generate_scheme_files(schemes):
    for scheme_name, sections in schemes.items():
        with open(f'{scheme_name}.toml', 'w') as file:
            for section, lines in sections.items():
                file.write(f'[colors.{section}]\n')
                for line in lines:
                    file.write(f'{line}\n')
                file.write('\n')

# Read the content from the file (assuming content is stored in `data`)
with open('colorschemes.toml', 'r') as file:
    content = file.read()

schemes = parse_color_schemes(content)
generate_scheme_files(schemes)
