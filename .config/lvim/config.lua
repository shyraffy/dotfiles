-- Read the docs: https://www.lunarvim.org/docs/configuration
-- Example configs: https://github.com/LunarVim/starter.lvim
-- Video Tutorials: https://www.youtube.com/watch?v=sFA9kX-Ud_c&list=PLhoH5vyxr6QqGu0i7tt_XoVK9v-KvZ3m6
-- Forum: https://www.reddit.com/r/lunarvim/
-- Discord: https://discord.com/invite/Xb9B4Ny
lvim.plugins = {
    -- Themes
    {
        "navarasu/onedark.nvim",
        config = function()
            require("onedark").setup({
                style = "darker",
                transparent = false,
                ending_tildes = true,
                code_style = {
                    comments = 'italic',
                    keywords = 'bold',
                    functions = 'bold',
                    strings = 'bold',
                    variables = 'bold'
                },
            })
        end,
    },
    {
        'NTBBloodbath/doom-one.nvim'
    },
    {
        'vyfor/cord.nvim',
        branch = 'client-server',
        build = ':Cord fetch',
        opts = {
            editor = {

                client = 'lunarvim'
            }
        }, -- calls require('cord').setup()
    },
    { "ellisonleao/gruvbox.nvim", priority = 1000, config = true, opts = ... }
}

-- lvim.format_on_save.enabled = true
lvim.colorscheme = "doom-one"
lvim.transparent_window = true
vim.g.doom_one_transparent_background = true

-- Move text up and down
lvim.keys.visual_mode["<A-j>"] = ":m .+1<CR>=="
lvim.keys.visual_mode["<A-k>"] = ":m .-2<CR>=="
lvim.keys.visual_block_mode["J"] = ":move '>+1<CR>gv-gv"
lvim.keys.visual_block_mode["K"] = ":move '<-2<CR>gv-gv"
lvim.keys.visual_block_mode["<A-j>"] = ":move '>+1<CR>gv-gv"
lvim.keys.visual_block_mode["<A-k>"] = ":move '<-2<CR>gv-gv"
lvim.keys.visual_mode["p"] = '"_dP'

-- Navigate buffers
lvim.keys.normal_mode["<S-l>"] = ":bnext<CR>"
lvim.keys.normal_mode["<S-h>"] = ":bprevious<CR>"

-- Tmux find file
lvim.keys.normal_mode["<c-f>"] = "<cmd>silent !tmux neww tmux-sessionizer<CR>"

-- Remove highlighting when searching
lvim.keys.normal_mode["<c-s>"] = ":noh<cr>"

-- Terminal --
lvim.builtin["terminal"].open_mapping = [[<c-\>]]
-- Better terminal navigation
lvim.keys.term_mode["<C-h>"] = "<C-\\><C-N><C-w>h"
lvim.keys.term_mode["<C-j>"] = "<C-\\><C-N><C-w>j"
lvim.keys.term_mode["<C-k>"] = "<C-\\><C-N><C-w>k"
lvim.keys.term_mode["<C-l>"] = " <C-\\><C-N><C-w>l"

-- Center the view when scrolling half page up/down
lvim.keys.normal_mode["<C-d>"] = "<C-d>zz"
lvim.keys.normal_mode["<C-u>"] = "<C-u>zz"

-- Center view when scrolling
lvim.keys.normal_mode["n"] = "nzz"
lvim.keys.normal_mode["N"] = "Nzz"

-- Options
vim.opt.backup = false                          -- creates a backup file
vim.opt.clipboard = "unnamedplus"               -- allows neovim to access the system clipboard
vim.opt.cmdheight = 2                           -- more space in the neovim command line for displaying messages
vim.opt.completeopt = { "menuone", "noselect" } -- mostly just for cmp
vim.opt.conceallevel = 0                        -- so that `` is visible in markdown files
vim.opt.fileencoding = "utf-8"                  -- the encoding written to a file
vim.opt.hlsearch = true                         -- highlight all matches on previous search pattern
vim.opt.ignorecase = true                       -- ignore case in search patterns
vim.opt.mouse = "a"                             -- allow the mouse to be used in neovim
vim.opt.pumheight = 10                          -- pop up menu height
vim.opt.showmode = false                        -- we don't need to see things like -- INSERT -- anymore
vim.opt.showtabline = 2                         -- always show tabs
vim.opt.smartcase = true                        -- smart case
vim.opt.smartindent = true                      -- make indenting smarter again
vim.opt.splitbelow = true                       -- force all horizontal splits to go below current window
vim.opt.splitright = true                       -- force all vertical splits to go to the right of current window
vim.opt.swapfile = false                        -- creates a swapfile
vim.opt.termguicolors = true                    -- set term gui colors (most terminals support this)
vim.opt.timeoutlen = 1000                       -- time to wait for a mapped sequence to complete (in milliseconds)
vim.opt.undofile = true                         -- enable persistent undo
vim.opt.updatetime = 300                        -- faster completion (4000ms default)
vim.opt.writebackup = false                     -- if a file is being edited by another program (or was written to file while editing with another program), it is not allowed to be edited
vim.opt.expandtab = true                        -- convert tabs to spaces
vim.opt.shiftwidth = 4                          -- the number of spaces inserted for each indentation
vim.opt.tabstop = 4                             -- insert 4 spaces for a tab
vim.opt.cursorline = false                      -- highlight the current line
vim.opt.number = true                           -- set numbered lines
vim.opt.relativenumber = true                   -- set relative numbered lines
vim.opt.numberwidth = 4                         -- set number column width to 2 {default 4}
vim.opt.signcolumn =
"yes"                                           -- always show the sign column, otherwise it would shift the text each time
vim.opt.wrap = true                             -- don't display lines as one long line
vim.opt.scrolloff = 8                           -- is one of my fav
vim.opt.sidescrolloff = 8
vim.opt.guifont = "monospace:h17"               -- the font used in graphical neovim applications

-- Lualine
lvim.builtin.lualine.options = {
    icons_enabled = true,
    theme = 'auto',
    component_separators = { left = '', right = '' },
    section_separators = { left = '', right = '' },
    disabled_filetypes = {
        statusline = {},
        winbar = {},
    },
    ignore_focus = {},
    always_divide_middle = true,
    globalstatus = false,
    refresh = {
        statusline = 1000,
        tabline = 1000,
        winbar = 1000,
    }
}
lvim.builtin.lualine.sections = {
    lualine_a = { 'mode' },
    lualine_b = { 'branch', 'diff', 'diagnostics' },
    lualine_c = { 'filename' },
    lualine_x = { 'encoding', 'fileformat', 'filetype' },
    lualine_y = { 'progress' },
    lualine_z = { 'location' }
}
lvim.builtin.lualine.inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = { 'filename' },
    lualine_x = { 'location' },
    lualine_y = {},
    lualine_z = {}
}

-- Telescope
-- Show previewer when searching git files with default <leader>f
lvim.builtin.which_key.mappings["f"] = {
    function ()
      require("lvim.core.telescope.custom-finders").find_project_files {layout_config = {width = 0.95, height = 0.95}}
    end,
  "Find File"
}
-- Show previewer when searching buffers with <leader>bf
lvim.builtin.which_key.mappings.b.f = {
  "<cmd>Telescope buffers<cr>",
  "Find"
}
lvim.builtin.telescope.defaults.layout_strategy = "horizontal"
lvim.builtin.telescope.defaults.borderchars = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" }

lvim.builtin.treesitter.indent = {
    enable = true,
    disable = { "c", "cpp" },
}


