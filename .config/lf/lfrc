# Basic settings
set ignorecase true
set drawbox true
set icons true
set preview true
set autoquit true

# Image preview
set previewer ~/.config/lf/preview
set cleaner ~/.config/lf/cleaner
set ratios 1:2:3

# Archive bindings
# Taken from https://github.com/ericmurphyxyz/dotfiles
cmd unarchive ${{
  case "$f" in
      *.zip) unzip "$f" ;;
      *.tar.gz) tar -xzvf "$f" ;;
      *.tar.bz2) tar -xjvf "$f" ;;
      *.tar) tar -xvf "$f" ;;
      *) echo "Unsupported format" ;;
  esac
}}

cmd mkdir ${{
  printf "Directory Name: "
  read ans
  mkdir $ans
}}


cmd mkfile ${{
  printf "File Name: "
  read ans
  $EDITOR $ans
}}

cmd fzf_jump ${{
    res="$(find . -maxdepth 1 | fzf --reverse --header='Jump to location' | sed 's/\\/\\\\/g;s/"/\\"/g')"
    if [ -d "$res" ] ; then
        cmd="cd"
    elif [ -f "$res" ] ; then
        cmd="select"
    else
        exit 0
    fi
    lf -remote "send $id $cmd \"$res\""
}}


# Bindings
# Remove some defaults
map m
map o
map n
map "'"
map '"'
map d
map c
map e
map f

map au unarchive
map md mkdir
map <c-f> :fzf_jump

# Basic Functions
map . set hidden!
map DD delete
map p paste
map x cut
map y copy
map <enter> open
map mf mkfile
map md mkdir
map r rename
map H top
map L bottom
map R reload
map C clear
map U unselect

# Fast movement
map gd cd ~/Documents
map gc cd ~/.config
map gD cd ~/Downloads
