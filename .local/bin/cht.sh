#!/usr/bin/env bash

# Original by ThePrimeagen in YT.

languages=$(echo "rust lua c python zig" | tr ' ' '\n')
core_utils=$(echo "xargs zip dd ffmpeg awk sed" | tr ' ' '\n')

selected=$(echo -e "$languages\n$core_utils" | fzf)
if [[ -z $selected ]]; then
    exit 0
fi
read -p "Query: " query


if printf $languages | grep -qs $selected; then
    query=`echo $query | tr ' ' '+'`
    tmux neww bash -c "echo \"curl cht.sh/$selected/$query/\" & curl cht.sh/$selected/$query & while [ : ]; do sleep 1; done"
else
    tmux neww bash -c "curl -s cht.sh/$selected~$query | less"
fi
