#!/bin/bash

DIR_COLORSCRIPTS="$HOME/.local/bin/colorscripts"
LS_CMD="$(command -v ls) ${DIR_COLORSCRIPTS}"
list_colorscripts="$($LS_CMD | xargs -I $ basename $ | cut -d ' ' -f 1 | nl)"
length_colorscripts="$($LS_CMD | wc -l)"

function _random() {
    declare -i random_index=$RANDOM%$length_colorscripts
    [[ $random_index -eq 0 ]] && random_index=1

    random_colorscript="$(echo  "${list_colorscripts}" | sed -n ${random_index}p \
        | tr -d ' ' | tr '\t' ' ' | cut -d ' ' -f 2)"
    # echo "${random_colorscript}"
    exec "${DIR_COLORSCRIPTS}/${random_colorscript}"
}

_random
