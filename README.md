# My Dotfiles 

## My Window Managers

- [dwm](https://gitlab.com/shyraffy/dwm-shyraffy)
- [qtile](https://gitlab.com/shyraffy/dotfiles#qtile)

## Qtile

![Qtile](.screenshots/qtile.png)

[Qtile](http://www.qtile.org/) is a tiling window manager written in Python. Qtile just happened to be the first tiling window manager I stumbled upon on when I took the red pill and started using Arch.

I took my config from [Antonio Sarosi](https://github.com/antoniosarosi) and made a few minor changes. I recommend you check his dotfiles for other programs and window managers config files. He also has a full tutorial on how to set up everything from scratch.

**Make sure you have a [Nerd Font](https://www.nerdfonts.com/) for the icons to display properly!** I'm using ShureTechMono and UbuntuMono for some widgets.

## Alacritty

[Alacritty](https://github.com/alacritty/alacritty) is a GPU accelerated terminal emulator written in Rust. I've been using this terminal forever and never had a reason to switch.
The config file is pretty much based on [Derek Taylor's config](https://gitlab.com/dwt1), although I took the liberty to search for other colorschemes in github. He also has a lot of cool config files for other window managers and programs, so make sure to check him out!

## Neovim

![Neovim](.screenshots/nvim.png)

My nvim config uses the Lua API and the [packer plugin manager](https://github.com/wbthomason/packer.nvim).

The config comes from Christian Chiarulli's [Neovim from scratch](https://github.com/LunarVim/Neovim-from-scratch) repo. There are several features from this repo that I didn't add because I haven't gotten around to configure it completely, and therefore I don't recommend taking my config. You can clone his repo directly or watch the [playlist](https://youtube.com/playlist?list=PLhoH5vyxr6Qq41NFL4GvhFp-WLd5xzIzZ) he has on YouTube.

If you want to know what plugins I'm using, they are located at ```.config/nvim/lua/user/plugins.lua```.

## Rofi

[Rofi](https://github.com/davatorium/rofi) is a menu that can do many things, from launching applications to serving as a calculator.
I use it for launching programs and commands. The config file is practically the same as [ericmurphyxyz](https://github.com/ericmurphyxyz)'s

The only issue here is that there are some programs that I cannot launch from rofi because they need sudo privileges, like the nvidia-settings panel. This could be solved with some kind of sudo GUI frontend, but I haven't done much research. Let me know if you find a way!

### Power menu

I'm also using rofi as a power menu because I didn't want to turn off my PC with the terminal. As I said, rofi can do many things.
I searched for some way of having a menu for this purpose and found [this video](https://www.youtube.com/watch?v=v8w1i3wAKiw&t=418s), also from ericmurphyxyz. I placed the script in ```.local/bin``` and added it to my path variable, so I could call it from a widget in qtile like this:

```python
# .config/qtile/settings/widget.py

widget.TextBox(
        text="襤 ", # Icon: nf-mdi-power
        mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn('powermenu')}
    ),

```

## Zsh

Using bash would be boring, so I went with Zsh as my main shell.

**If you want to use my config, make sure to set your ```ZDOTDIR``` variable like this so ZSH knows that the config is in ```.config``` instead of your home directory!**

```bash
export ZDOTDIR=$HOME/.config/zsh
```

My config is all based on Christian Chiarulli's [video](https://www.youtube.com/watch?v=bTLYiNvRIVI) about his Zsh config. I went with his video because he explains how to make Zsh look and behave really cool without installing a bloated plugin manager like Oh My Zsh. Please go check him out!

In his config, we use [fzf](https://github.com/junegunn/fzf) to search for previous commands. If you don't want this, remove these lines from ```.config/zsh/.zshrc```:

```bash
# FZF 
[ -f /usr/share/fzf/completion.zsh ] && source /usr/share/fzf/completion.zsh
[ -f /usr/share/fzf/key-bindings.zsh ] && source /usr/share/fzf/key-bindings.zsh
[ -f /usr/share/doc/fzf/examples/completion.zsh ] && source /usr/share/doc/fzf/examples/completion.zsh
[ -f /usr/share/doc/fzf/examples/key-bindings.zsh ] && source /usr/share/doc/fzf/examples/key-bindings.zsh
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
compinit
```

I also took some bites from [Luke Smith's config](https://www.youtube.com/watch?v=eLEo4OQ-cuQ&t=185s). His config also works without any plugin manager and you can learn some cool stuff from his video.  
In his config, we use ctrl+o to open [lf](https://github.com/gokcehan/lf), a terminal file manager. If you don't want this, remove these lines from ```.config/zsh/.zshrc```

```bash
# Use lf to switch directories and bind it to ctrl-o
lfcd () {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
    fi
}

bindkey -s '^o' 'lfcd\n'
```

## Colorscripts

As you can see in the screenshot, every time I open my terminal, a random color art is displayed. This is from Derek Taylor's [shell color scripts](https://gitlab.com/dwt1/shell-color-scripts) repo.

You could just install them from [AUR](https://aur.archlinux.org/packages/shell-color-scripts), but there were a lot of scripts I didn't like, and Derek hasn't implemented any wood way of whitelisting yet. My workaround was to download the ones I like, and place them in ```.local/bin/colorcripts```. Then, because I want the random functionality, I took part of his colorscript.sh code and placed it in ```.local/bin/colorscript.sh``` like so:

```bash
#!/bin/bash

DIR_COLORSCRIPTS="$HOME/.local/bin/colorscripts"
LS_CMD="$(command -v ls) ${DIR_COLORSCRIPTS}"
list_colorscripts="$($LS_CMD | xargs -I $ basename $ | cut -d ' ' -f 1 | nl)"
length_colorscripts="$($LS_CMD | wc -l)"

function _random() {
    declare -i random_index=$RANDOM%$length_colorscripts
    [[ $random_index -eq 0 ]] && random_index=1

    random_colorscript="$(echo  "${list_colorscripts}" | sed -n ${random_index}p \
        | tr -d ' ' | tr '\t' ' ' | cut -d ' ' -f 2)"
    # echo "${random_colorscript}"
    exec "${DIR_COLORSCRIPTS}/${random_colorscript}"
}

_random
```

After that, I just added this script to my path variable, and added this line to ```.config/zsh/.zshrc```:

```bash
# Choose a random color script
colorscript.sh
```

I know very little about bash scripting, and I really don't understand how the function works, so full credits to him.

## Wallpaper

If you are reading this, it's because you are a person of culture and you want my wallpaper. I downloaded it from [here](https://wallhaven.cc/w/y8zw2l).

## License

[MIT](https://choosealicense.com/licenses/mit/)
